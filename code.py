import board
import digitalio
import time
import usb_hid
import json
from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keycode import Keycode
from adafruit_hid.consumer_control_code import ConsumerControlCode


with open('config.json', 'r') as conf:
  data = json.load(conf)

var_key1 = Keycode.A
var_key2 = Keycode.B
var_key3 = Keycode.C
var_key4 = Keycode.D
var_key5 = Keycode.E
var_key6 = Keycode.F
var_key7 = Keycode.G
var_key8 = Keycode.H
var_key9 = Keycode.I

keyboard = Keyboard(usb_hid.devices)
B1 = board.GP0
B2 = board.GP2
B3 = board.GP3
B4 = board.GP4
B5 = board.GP5
B6 = board.GP6
B7 = board.GP7
B8 = board.GP8
B9 = board.GP9

B1 = digitalio.DigitalInOut(B1)
B1.direction = digitalio.Direction.INPUT
B1.pull = digitalio.Pull.DOWN

B2 = digitalio.DigitalInOut(B2)
B2.direction = digitalio.Direction.INPUT
B2.pull = digitalio.Pull.DOWN

B3 = digitalio.DigitalInOut(B3)
B3.direction = digitalio.Direction.INPUT
B3.pull = digitalio.Pull.DOWN

B4 = digitalio.DigitalInOut(B4)
B4.direction = digitalio.Direction.INPUT
B4.pull = digitalio.Pull.DOWN

B5 = digitalio.DigitalInOut(B5)
B5.direction = digitalio.Direction.INPUT
B5.pull = digitalio.Pull.DOWN

B6 = digitalio.DigitalInOut(B6)
B6.direction = digitalio.Direction.INPUT
B6.pull = digitalio.Pull.DOWN

B7 = digitalio.DigitalInOut(B7)
B7.direction = digitalio.Direction.INPUT
B7.pull = digitalio.Pull.DOWN

B8 = digitalio.DigitalInOut(B8)
B8.direction = digitalio.Direction.INPUT
B8.pull = digitalio.Pull.DOWN

B9 = digitalio.DigitalInOut(B9)
B9.direction = digitalio.Direction.INPUT
B9.pull = digitalio.Pull.DOWN

B1_bool= False
B2_bool= False
B3_bool= False
B4_bool= False
B5_bool= False
B6_bool= False
B7_bool= False
B8_bool= False
B9_bool= False


while True:
    if B1.value:
        keyboard.press(var_key1)
        time.sleep(0.15)
        keyboard.release(var_key1)
        B1_bool = not B1_bool

    if B2.value:
        keyboard.press(var_key2)
        time.sleep(0.15)
        keyboard.release(var_key2)
        B2_bool = not B2_bool

    if B3.value:
        keyboard.press(var_key3)
        time.sleep(0.15)
        keyboard.release(var_key3)
        B3_bool = not B3_bool

    if B4.value:
        keyboard.press(var_key4)
        time.sleep(0.15)
        keyboard.release(var_key4)
        B4_bool = not B4_bool

    if B5.value:
        keyboard.press(var_key5)
        time.sleep(0.15)
        keyboard.release(var_key5)
        B5_bool = not B5_bool

    if B6.value:
        keyboard.press(var_key6)
        time.sleep(0.15)
        keyboard.release(var_key6)
        B6_bool = not B6_bool

    if B7.value:
        keyboard.press(var_key7)
        time.sleep(0.15)
        keyboard.release(var_key7)
        B7_bool = not B7_bool

    if B8.value:
        keyboard.press(var_key8)
        time.sleep(0.15)
        keyboard.release(var_key8)
        B8_bool = not B8_bool

    if B9.value:
        keyboard.press(var_key9)
        time.sleep(0.15)
        keyboard.release(var_key9)
        B9_bool = not B9_bool


    time.sleep(0.1)
